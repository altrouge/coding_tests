#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <functional>

/* related to https://www.codingame.com/training/hard/bender-episode-3 */

using namespace std;

/**
 * Auto-generated code below aims at helping you parse
 * the standard input according to the problem statement.
 **/

void RANSAC(std::vector<float> data)
{
}

double linear(double n)
{
    return n;
}

double mlog(double n)
{
    return std::log(n);
}

struct s_data
{
    s_data() {}
    s_data(double numin, double tin): num(numin), t(tin) {}
    double num;
    double t;
};

struct test_fun
{
    test_fun(){}
    test_fun(std::function<double(double)> funin, string namein, double distancein = 0):
        fun(funin),
        distance(distancein),
        name(namein)
    {}

    std::function<double(double)> fun;
    double distance;
    string name;
};

double computeDistance(const std::vector<s_data>& datain, std::function<double(double)> fun, bool output_data = false)
{
    std::vector<s_data> data = datain;

    double sum = 0;
    for(s_data& datum: data)
    {
        sum += datum.t / fun(datum.num);
    }

    double mean = sum / data.size();
    std::cerr << "mean: " << mean << std::endl;

    double distance = 0;
    for(s_data& datum: data)
    {
        distance += std::abs(datum.t - mean * fun(datum.num));
        if(output_data)
        {
            std::cout << datum.num << "," << datum.t << "," << mean*fun(datum.num) << std::endl;
        }
    }

    return distance;
}

double computeDistanceOutlier(const std::vector<s_data>& datain, std::function<double(double)> fun, bool output_data = false)
{
    std::vector<s_data> data = datain;

    double sum = 0;
    std::vector<float> means;
    for(s_data& datum: data)
    {
        sum += datum.t / fun(datum.num);
        means.push_back(datum.t/fun(datum.num));
    }

    double mean = sum / data.size();
    std::cerr << "mean: " << mean << std::endl;
    double sd = 0;
    for(double m: means)
    {
        sd += std::abs(m - mean);
    }
    sd = sd/means.size();

    std::vector<s_data> inliers;

    // we iterate
    float coeff = 5;
    for(int k = 0; k < 5; ++k)
    {
        inliers.clear();
        float newmean = 0;
        float newsd = 0;
        for(int i = 0; i < means.size(); ++i)
        {
            //std::cout << "coeff: " << coeff << "sd: " << sd << std::endl;
            if( std::abs(means[i] - mean)  < coeff * sd)
            {
                inliers.push_back(data[i]);
                newmean += means[i];
            }
        }
        if(inliers.size() > 0.3*data.size())
        {
            mean = newmean/inliers.size();
            for(s_data& inlier: inliers)
            {
                sd += std::abs(inlier.t/fun(inlier.num) - mean);
            }
            sd = sd/means.size();
        }
        if(inliers.size() < 0.9 * data.size())
        {
            //std::cerr << "growing coeff" << std::endl;
            coeff = 3*coeff;
            k-=1;
        }
    }
    std::cerr << "inliers: " << inliers.size() << std::endl;
    // calculate new mean:
    sum = 0;
    for(s_data& datum: inliers)
    {
        sum+= datum.t / fun(datum.num);
    }
    mean = sum/ inliers.size();

    double distance = 0;
    for(s_data& datum: inliers)
    {
        distance += std::abs(datum.t - mean * fun(datum.num));
        if(output_data)
        {
            std::cout << datum.num << "," << datum.t << "," << mean*fun(datum.num) << std::endl;
        }
    }

    return distance;
}

int main()
{

    std::vector<s_data> data;
    int N;
    cin >> N; cin.ignore();
    for (int i = 0; i < N; i++) {
        int num;
        int t;
        cin >> num >> t; cin.ignore();
        data.push_back(s_data(num,t));
    }

    std::vector<test_fun> functions;
    functions.push_back(test_fun([](double n){return n;}, "O(n)"));
    functions.push_back(test_fun([](double n){return 1.f;}, "O(1)"));
    functions.push_back(test_fun([](double n){return std::log(n);}, "O(log n)"));
    functions.push_back(test_fun([](double n){return n*std::log(n);}, "O(n log n)"));
    functions.push_back(test_fun([](double n){return n*n*std::log(n);}, "O(n^2 log n)"));
    functions.push_back(test_fun([](double n){return n*n;}, "O(n^2)"));
    functions.push_back(test_fun([](double n){return n*n*n;}, "O(n^3)"));
    functions.push_back(test_fun([](double n){return std::pow(2,n);}, "O(2^n)"));

    for(auto& function: functions)
    {
        function.distance = computeDistanceOutlier(data, function.fun);
    }

    bool init = false;
    std::string result = "none";
    double distance = 0;
    std::cerr << "size: " << functions.size() << std::endl;
    for(int i = 0; i < functions.size(); ++i)
    {
        std::cerr << functions[i].name << ": " << functions[i].distance << std::endl;
        if(!init)
        {
            distance = functions[i].distance;
            result = functions[i].name;
            init = true;
        }
        else
        {
            if(functions[i].distance < distance)
            {
                distance = functions[i].distance;
                result = functions[i].name;
            }
        }
    }

    //functions.push_back(test_fun(std::log, "O(log n)"));
    // we normalize the data.
    //float const_dist =
    //, log_dist, lin_dist, nlnn_dist, n2_dist, n2lnn_dist, n3dist, expdist;

    // Write an action using cout. DON'T FORGET THE "<< endl"
    // To debug: cerr << "Debug messages..." << endl;
    std::cerr << "result: " << result << std::endl;

    // we debug in separate file:
    test_fun function = functions.back();
    std::cerr << computeDistanceOutlier(data, function.fun) << std::endl;
    //cout << result << endl;
}
