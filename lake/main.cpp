#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <deque>

using namespace std;

/* https://www.codingame.com/training/hard/surface */

/**
 * Auto-generated code below aims at helping you parse
 * the standard input according to the problem statement.
 **/

class Matrix
{
    public:
        Matrix(int width = 0, int height = 0)
        {
            m_array.resize(height);
            for(auto& line: m_array)
                line.resize(width);
            m_width = width;
            m_height = height;
        }

        inline int width() { return m_width; }
        inline int height() { return m_height; }
        inline long int operator()(int i, int j) const { return m_array[i][j]; }
        inline long int& operator()(int i, int j) { return m_array[i][j]; }

        void array(const std::vector<std::vector<long int>>& arr)
        {
            m_array = arr;
            m_height = arr.size();
            m_width = 0;
            if(m_height > 0)
            {
                m_width = arr[0].size();
            }
        }

        void array(std::vector<std::vector<long int>>&& arr)
        {
            m_array = arr;
            m_height = arr.size();
            m_width = 0;
            if(m_height > 0)
            {
                m_width = arr[0].size();
            }
        }

    public:
        int m_width;
        int m_height;
    private:
        std::vector<std::vector<long int>> m_array;
};

void GrowMap(Matrix& matrix, int i, int j, std::vector<std::pair<int,int>>& treated_indexes)
{
    if(i >= 0 && j >= 0 && j < matrix.width() && i < matrix.height())
    {
        if(matrix(i,j) == -1)
        {
            matrix(i,j) = -2;
            treated_indexes.push_back(std::pair<int, int>(i,j));
            GrowMap(matrix, i+1,j, treated_indexes);
            GrowMap(matrix, i,j+1, treated_indexes);
            GrowMap(matrix, i-1,j, treated_indexes);
            GrowMap(matrix, i,j-1, treated_indexes);
        }
    }
}

long int GrowMap2(Matrix& matrix, int i, int j)
{
    long int result = 0;
    if(i >= 0 && j >= 0 && j < matrix.m_width && i < matrix.m_height)
    {
        if(matrix(i,j) == -1)
        {
            matrix(i,j) = -2;

            result += 1;

            result += GrowMap2(matrix, i+1,j);
            result += GrowMap2(matrix, i,j+1);
            result += GrowMap2(matrix, i-1,j);
            result += GrowMap2(matrix, i,j-1);
            std::cerr << "result: " << result << std::endl;
        }
    }
    return result;
}

long int GrowMap3(Matrix& matrix, int i, int j)
{
    typedef std::pair<int,int> index;
    long int result = 0;
    std::deque<index> to_treat;
    std::vector<index> lake;
    //to_treat.reserve(matrix.width()*matrix.height());
    lake.reserve(matrix.width()*matrix.height());
    to_treat.push_back(index(i,j));
    while(to_treat.size() != 0)
    {
        index current = to_treat.front();
        int i = current.first;
        int j = current.second;
        if(i >= 0 && j >= 0 && j < matrix.width() && i < matrix.height())
        {
            if(matrix(i,j) == -1)
            {
                matrix(i,j) = -2;
                lake.push_back(current);

                to_treat.push_back(index(i+1,j));
                to_treat.push_back(index(i,j+1));
                to_treat.push_back(index(i-1,j));
                to_treat.push_back(index(i,j-1));
            }
        }
        to_treat.pop_front();
    }

    result = lake.size();
    for(const auto& valid: lake)
    {
        matrix(valid.first, valid.second) = result;
    }

    return result;
}

long int QueryMap(Matrix& matrix, int i, int j)
{
    // first, if it is an already treated data or a land:
    if(matrix(i,j) != -1)
    {
        return matrix(i,j);
    }

    //otherwise:

    //std::vector<std::pair<int,int>> treated_indexes;
    //treated_indexes.reserve(matrix.width() * matrix.height());
    //GrowMap(matrix, i, j, treated_indexes);

    //long int size = treated_indexes.size();
    //for(auto& index: treated_indexes)
    //{
    //    matrix(index.first, index.second) = size;
    //}
    /*
    long int result = GrowMap2(matrix, i, j);
    for(int i = 0; i < matrix.height(); ++i)
        for(int j = 0; j < matrix.width(); ++j)
            if(matrix(i,j) == -2)
                matrix(i,j) = result;
    */
    long int result = GrowMap3(matrix, i, j);
    return result;
}

int main()
{
    int L;
    cin >> L; cin.ignore();
    int H;
    cin >> H; cin.ignore();

    //Matrix matrix(L, H);
    std::vector<std::vector<long int>> result;
    for (int i = 0; i < H; i++) {
        string row;
        getline(cin, row);
        std::vector<long int> curline;
        for(char& c: row)
        {
            if(c == '#')
            {
                curline.push_back(0);
            }
            else if(c == 'O')
            {
                curline.push_back(-1);
            }
        }
        result.push_back(curline);
    }

    Matrix matrix;
    matrix.array(result);

    std::vector<std::pair<int, int>> queries;
    int N;
    cin >> N; cin.ignore();
    for (int i = 0; i < N; i++) {
        int X;
        int Y;
        cin >> X >> Y; cin.ignore();
        queries.push_back(std::pair<int,int>(X,Y));
    }
    //for (int i = 0; i < N; i++) {
    for (auto query: queries)
    {
        //long int result = QueryMap(matrix, query.first, query.second);
        long int result = QueryMap(matrix, query.second, query.first);

        // Write an action using cout. DON'T FORGET THE "<< endl"
        // To debug: cerr << "Debug messages..." << endl;

        cout << result << endl;
    }
}
