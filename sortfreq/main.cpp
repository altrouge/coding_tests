// see http://www.practice.geeksforgeeks.org/problem-page.php?pid=493

#include <algorithm>
#include <iostream>
#include <string>
#include <sstream>
#include <vector>

std::string FreqSort(std::vector<int> array)
{
    std::sort(array.begin(), array.end());

    typedef std::pair<int, int> ValueFrequency;
    std::vector<ValueFrequency> results;

    int curval = 0;
    if(array.size() != 0)
    {
        curval = array[0];
    }

    int inc = 0;
    for(int value: array)
    {
        if(value == curval)
        {
            inc++;
        }

        if(value != curval)
        {
            results.push_back(ValueFrequency(curval, inc));
            curval = value;
            inc = 1;
        }
    }

    results.push_back(ValueFrequency(curval, inc));

    std::sort(results.begin(), results.end(),
            [](ValueFrequency x, ValueFrequency y) { return x.second > y.second || (x.second == y.second && x.first < y.first); });

    std::stringstream os;
    for(int i = 0; i < results.size() - 1; ++i)
    {
        //std::cerr << results[i].first << " " << results[i].second << std::endl;
        for(int j = 0; j < results[i].second; ++j)
        {
            os << results[i].first << " ";
        }
    }

    //std::cerr << results.back().first << " " << results.back().second << std::endl;

    for(int j = 0; j < results.back().second - 1; ++j)
    {
        os << results.back().first << " ";
    }

    os << results.back().first;

    return os.str();

}

int main(int argc, char** argv)
{

    int n = 0;
    std::cin >> n;
    for(int i = 0; i < n; ++i)
    {
        int array_size = 0;
        std::cin >> array_size;
        std::vector<int> array;
        array.resize(array_size);

        for(int& value: array)
        {
            std::cin >> value;
        }

        std::cout << FreqSort(array) << std::endl;

    }
    return 0;
}
