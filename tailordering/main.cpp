#include <iostream>
#include <vector>
#include <string>

/* http://www.practice.geeksforgeeks.org/problem-page.php?pid=173 */
// TODO: Not complete. See when we can get stuck, and what does the graph look like ?
//
bool isTailOrderable(const std::vector<std::string>& strvec)
{
    int first_alphabet[26];
    int last_alphabet[26];
    char a = 'a';

    for(int i = 0; i < 26; ++i)
    {
        first_alphabet[i] = 0;
        last_alphabet[i] = 0;
    }

    for(std::string str: strvec)
    {
        first_alphabet[static_cast<int>(str.front()) - a]++;
        last_alphabet[static_cast<int>(str.back()) - a]++;
    }

    //
    bool result = true;
    int plusdiff = 0;
    int minusdiff = 0;

    for(int i = 0; i < 26; ++i)
    {
        //std::cout << "alphabet " << i << ":" << static_cast<int>(first_alphabet[i]) << "," << static_cast<int>(last_alphabet[i]) << std::endl;
        if(first_alphabet[i] == last_alphabet[i])
        {
        }
        else if(first_alphabet[i] == last_alphabet[i] + 1)
        {
            plusdiff++;
        }
        else if(first_alphabet[i] + 1 == last_alphabet[i])
        {
            minusdiff++;
        }
        else
        {
            result = false;
            break;
        }

        if(plusdiff > 1 || minusdiff > 1)
        {
            result = false;
            break;
        }
    }

    //std::cout << "plusdiff, minusdiff, result: " << plusdiff << "," << minusdiff << "," << result << std::endl;

    return result;
}

int main() {
	//code
	int test_cases;
	std::cin >> test_cases;
	for(int i = 0; i < test_cases; ++i)
	{
	    int vecsize;
	    std::cin >> vecsize;
	    std::vector<std::string> strvec(vecsize);
	    for(int j = 0; j < vecsize; ++j)
	    {
	        std::string res;
	        std::cin >> res;
	        strvec.push_back(res);
	    }

        //std::cout << "vec size: " << strvec.size() << std::endl;
	    if(isTailOrderable(strvec))
	    {
	        std::cout << "Head to tail ordering is possible." << std::endl;
	    }
	    else
	    {
	        std::cout << "Head to tail ordering is not possible." << std::endl;
	    }
	}

	return 0;
}
