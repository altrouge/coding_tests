#include <iostream>
#include <string>
#include <sstream>
#include <vector>
#include <cmath>
#include <algorithm>

/* http://www.practice.geeksforgeeks.org/problem-page.php?pid=165 */
// TODO: not complete, see how to reduce computation time

int Triplets(const std::vector<int>& array)
{
    std::vector<int> less_sum, more_sum;
    less_sum.resize(array.size());
    more_sum.resize(array.size());
    for(int i = 0; i < array.size(); ++i)
    {
        less_sum[i] = 0;
        for(int j = 0; j < i; ++j)
        {
            if(array[j] < array[i])
            {
                ++less_sum[i];
            }
        }
    }

    for(int i = array.size() - 1; i >= 0; --i)
    {
        more_sum[i] = 0;
        for(int j = array.size() - 1; j > i; --j)
        {
            if(array[i] < array[j])
            {
                ++more_sum[i];
            }
        }
    }

    int result = 0;
    for(int i = 0; i < array.size(); ++i)
    {
        //result += std::min(more_sum[i], less_sum[i]);
        result += more_sum[i] * less_sum[i];
    }

    return result;
}

int main() {

	int N;
	std::cin >> N;
	for(int i = 0; i < N; ++i)
	{
	    int size;
	    std::cin >> size;
	    std::vector<int> array;
	    for(int i = 0; i < size; ++i)
	    {
	        int j;
	        std::cin >> j;
	        array.push_back(j);
	    }

	    std::cout << Triplets(array) << std::endl;
	}

	return 0;
}
