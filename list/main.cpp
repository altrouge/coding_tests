#include <iostream>
#include <string>
#include <vector>
#include <algorithm>

using namespace std;

/* iirc https://www.codingame.com/training/hard/super-computer */
/* TODO: not complete, have a solution */

/**
 * Auto-generated code below aims at helping you parse
 * the standard input according to the problem statement.
 **/
struct UseComputer
{
    UseComputer(int startdatein = 0, int enddatein = 0):
        startdate(startdatein),
        enddate(enddatein)
        {}
    int startdate;
    int enddate;
};

inline bool intersect(UseComputer c1, UseComputer c2)
{
    return (c1.startdate <= c2.enddate && c2.startdate <= c1.enddate);
}

int numofcomputers(const std::vector<UseComputer>& computers, int pos, const std::vector<UseComputer>& already_used)
{
    int result = already_used.size();
    for(int i = pos; i < computers.size(); ++i)
    {
        bool posintersect = false;
        for(auto& com: already_used)
        {
            if(intersect(computers[i], com))
            {
                //std::cerr << "pos: " << pos << " i: " << i << " intersect: " << computers[pos].startdate << " " << computers[pos].enddate << " " << com.startdate << " " << com.enddate << std::endl;
                posintersect = true;
                break;
            }
        }

        if(
                !posintersect // if no intersection
                && (already_used.size() + computers.size() - i) > result
             )
        {
            std::vector<UseComputer> current = already_used;
            current.push_back(computers[i]);
            result = std::max(result, numofcomputers(computers, i + 1, current));
        }
    }

    /*
    for(auto& com: already_used)
    {
        std::cerr << com.startdate << " " << com.enddate << " ";
    }
    std::cerr << result << std::endl;
    */
    return result;
}

int numofcomputers(const std::vector<UseComputer>& computers, int pos, int end, int current_max_end)
{
    int result = 0;

    //int max_sub = 0;
    for(int i = pos; i < end; ++i)
    {
        if(computers[i].startdate > current_max_end)
        {
            // we add the current
            int submaxend = current_max_end;
            if(computers[i].enddate > submaxend)
            {
                submaxend = computers[i].enddate;
            }
            int subres = 1 + numofcomputers(computers, i+1, end, submaxend);

            if(subres > result)
            {
                std::cerr << computers[i].startdate << " " << computers[i].enddate << " " << submaxend << std::endl;
            }
            result = std::max(subres, result);
        }
        else if(computers[i].startdate > computers[pos].enddate)
        {
            break;
        }
        //bool posintersect = false;
    }

    return result;
}

int main()
{
    std::vector<UseComputer> usecomputers;
    int N;
    cin >> N; cin.ignore();
    for (int i = 0; i < N; i++) {
        int J;
        int D;
        cin >> J >> D; cin.ignore();
        usecomputers.push_back(UseComputer(J,J + D - 1));
    }

    std:cerr << "sorting data ... " << std::endl;
    std::sort(usecomputers.begin(), usecomputers.end(),
            [](const UseComputer& c1, const UseComputer& c2)
            {
            return (c1.startdate < c2.startdate || (c1.startdate == c2.startdate && c1.enddate < c2.enddate));
            }
            );
    std::cerr << "data sorted ... " << std::endl;

    // we remove multiple inputs:
    for(int i = 0; i < usecomputers.size() - 1; ++i)
    {
        if(usecomputers[i].enddate >= usecomputers[i+1].enddate)
        {
            usecomputers.erase(usecomputers.begin() + i);
            i--;
        }
    }

    // we remove multiple inputs:
    std::vector<std::pair<int,int>> separated_index;

    std::cerr << "remaining vectors: " << usecomputers.size() << std::endl;

    int start = 0;
    for(int i = 0; i < usecomputers.size() - 1; ++i)
    {
        if(usecomputers[i].enddate < usecomputers[i+1].startdate)
        {
            separated_index.push_back(std::pair<int,int>(start, i+1));
            start = i+1;
        }
    }
    separated_index.push_back(std::pair<int,int>(start, usecomputers.size()));

    int result = 0;

    std::cerr << "separated indexes: " << separated_index.size() << std::endl;
    for(const std::pair<int,int>& pair: separated_index)
    {
        //std::cout << "current pair: " << pair.first << " " << pair.second << std::endl;
        result += numofcomputers(usecomputers, pair.first, pair.second, 0);
    }
    //std::vector<UseComputer> empty;
    //int result = numofcomputers(usecomputers, 0, empty);

    // Write an action using cout. DON'T FORGET THE "<< endl"
    // To debug: cerr << "Debug messages..." << endl;

    std::cout << result << endl;
}
