#include <iostream>
#include <string>
#include <sstream>
#include <vector>
#include <cmath>

/* Solve problem: http://www.practice.geeksforgeeks.org/problem-page.php?pid=417 */

std::string SortArray(std::vector<int>& array)
{
    std::stringstream result;
    int max_num = 102;
    // we add the in the array name.
    for(int i = 0; i < array.size(); ++i)
    {
        array[i] += max_num * (array[array[i]] % max_num);
    }

    for(int& i: array)
    {
        i = std::floor(i/max_num);
    }

    if(array.size() > 0)
        result << array[0];

    for(int i = 1; i < array.size(); ++i)
    {
        result << " " << array[i];
    }

    return result.str();
}

int main() {

    // two methods:
    // 1) treat data sequentially, but we do not know if there are several loops
    // 2) cheat and save multiple data in one int.
	int N;
	std::cin >> N;
	for(int i = 0; i < N; ++i)
	{
	    int size;
	    std::cin >> size;
	    std::vector<int> array;
	    for(int i = 0; i < size; ++i)
	    {
	        int j;
	        std::cin >> j;
	        array.push_back(j);
	    }

	    std::cout << SortArray(array) << std::endl;
	}

	return 0;
}
