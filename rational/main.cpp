#include <iostream>
#include <vector>
#include <sstream>
#include <cmath>
#include <algorithm>
//using namespace std;

/* Solve problem: http://www.practice.geeksforgeeks.org/problem-page.php?pid=514 */

struct frac
{
    frac(int numin = 0, int denumin = 1)
    {
        num = numin;
        denum = denumin;
    }
    int num;
    int denum;
};

std::string DisplayFraction(frac fr)
{
    int num = fr.num;
    int denum = fr.denum;

    //std::cerr << "num: " << num << " denum: " << denum << std::endl;

    if(denum == 0)
        return "";

    std::stringstream integerpart;
    integerpart << std::floor(num/denum);
    if(num % denum == 0)
    {
    }
    else
    {
        integerpart << ".";

        std::vector<int> rests;
        std::vector<int> results;
        //results.push_back(num);
        rests.push_back(num % denum);
        //results.push_back(std::floor(10 * rests.back() / denum));

        int first_repeat = -1;
        bool repeat = false;
        while(!repeat)
        {
            int rest = (10 * rests.back()) % denum;
            int result = std::floor(10 * rests.back() / denum);
            if(rest == 0)
            {
                //std::cerr << "rest: " << rest << std::endl;
                //repeat = true;
                results.push_back(result);
                break;
            }

            for(int i = 0; i < rests.size(); ++i)
            {
                if(rests[i] == rest)
                {
                    // we have the first index that matches the rest.
                    first_repeat = i;
                    repeat = true;
                    break;
                }

            }

            results.push_back(result);
            rests.push_back(rest);
        }

        if(first_repeat == -1)
        {
            for(int res: results)
            {
                integerpart << res;
            }
        }
        else
        {
            /*
            std::cerr << "first repeat: " << first_repeat << std::endl;

            std::cerr << "rests: ";
            for(int i: rests)
            {
                std::cerr << i << " ";
            }
            std::cerr << std::endl;
            */

            for(int i = 0; i < first_repeat; ++i)
            {
                integerpart << results[i];
            }
            integerpart << "(";

            for(int i = first_repeat; i < results.size(); ++i)
            {
                integerpart << results[i];
            }

            integerpart << ")";
        }

    }

    return integerpart.str();
    // now the rest:

}

int main()
{

    int number_of_inputs;


    std::vector<frac> fractions;
    std::cin >> number_of_inputs;
    for(int i = 0; i < number_of_inputs; ++i)
    {
        int num, denum;
        std::cin >> num;
        std::cin >> denum;
        fractions.push_back(frac(num,denum));
    }

    for(frac fraction: fractions)
    {
        std::cout << DisplayFraction(fraction) << std::endl;
    }
    //code
    return 0;
}
